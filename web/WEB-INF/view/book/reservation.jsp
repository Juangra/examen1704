
<%@page import="model.Book"%>
<%@page import="java.util.Iterator"%>
<%@page import="java.util.ArrayList"%>
<jsp:useBean id="books" class="java.util.ArrayList" scope="request"/>  


<!DOCTYPE html>
<%@include file="/WEB-INF/view/header.jsp" %>

<div id="content">
    <h1>Lista de libros reservados</h1>
    <p>
        <a href="<%= request.getContextPath()%>/book/create">Nuevo libro</a>
    </p>
    <table>
        <tr>
            <th>Id</th>
            <th>T�tulo</th>
            <th>P�ginas</th> 
        </tr>
        <%        Iterator<model.Book> iterator = books.iterator();
            while (iterator.hasNext()) {
                Book book = iterator.next();%>
        <tr>
            <td><%= book.getId()%></td>
            <td><%= book.getTitle()%></td>
            <td><%= book.getPages()%></td>
 
            <td> 
                <a href="<%= request.getContextPath() + "/book/view/" + book.getId()%>"> Ver </a>
            </td>
        </tr>
        <%
            }
        %>          
    </table>
    <a href="<%= request.getContextPath() + "/book/index/1" %>"> 1</a>
    <a href="<%= request.getContextPath() + "/book/index/2" %>"> 2</a>
    <a href="<%= request.getContextPath() + "/book/index/3" %>"> 3</a>
    <a href="<%= request.getContextPath() + "/book/index/4" %>"> 4</a>
    <a href="<%= request.getContextPath() + "/book/index/5" %>"> 5</a>
    <a href="<%= request.getContextPath() + "/book/index/6" %>"> 6</a>
    <a href="<%= request.getContextPath() + "/book/index/7" %>"> 7</a>
    <a href="<%= request.getContextPath() + "/book/index/8" %>"> 8</a>
    <a href="<%= request.getContextPath() + "/book/index/9" %>"> 9</a>
    <a href="<%= request.getContextPath() + "/book/index/10" %>"> 10</a>
    <a href="<%= request.getContextPath() + "/book/index/11" %>"> 11</a>
    <a href="<%= request.getContextPath() + "/book/index/12" %>"> 12</a>
    
    <br>

   
</div>
<%@include file="/WEB-INF/view/footer.jsp" %>
