/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Book;

/**
 *
 * @page alumno
 */
public class BookDAO {

    public static final String DB_DRIVER = "com.mysql.jdbc.Driver";
    public static final String DB_URL = "jdbc:mysql://192.168.92.132/examen1703";
//    public static final String DB_URL = "jdbc:mysql://10.2.25.35/examen1702";
    public static final String DB_USER = "root";
    public static final String DB_PASS = "root";
    private static final Logger LOG = Logger.getLogger(BookDAO.class.getName());

    protected Connection connection;

    public BookDAO() {
//        Class.forName("com.mysql.jdbc.Driver");
//        this.connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
    }

    public void connect() {
        try {
            Class.forName(DB_DRIVER);
            connection = DriverManager.getConnection(DB_URL, DB_USER, DB_PASS);
            LOG.log(Level.INFO, "BBDD conectada");
        } catch (ClassNotFoundException ex) {
            LOG.log(Level.INFO, "error conexión BBDD");
            LOG.log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            LOG.log(Level.INFO, "error conexión BBDD");
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    public void disconnect() {
        try {
            connection.close();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Book> getAll(int page, int elementsPerPage) {
        PreparedStatement stmt = null;
        ArrayList<Book> books = null;
        int offset = (page - 1) * elementsPerPage;
        
        try {
            this.connect();
            stmt = connection.prepareStatement("select * from books LIMIT " + elementsPerPage + " OFFSET " + offset);
            ResultSet rs = stmt.executeQuery();
            books = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                Book book = new Book();
                book.setId(rs.getInt("id"));
                book.setPages(rs.getInt("pages"));
                book.setTitle(rs.getString("title"));
                book.setYear(rs.getInt("year"));         

                books.add(book);
                LOG.info("Registro fila: " + i);
            }
            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return books;
    }

    public ArrayList<Book> getPages(int page) {
        int size = 15;
        PreparedStatement stmt = null;
        ArrayList<Book> books = null;
        try {
            this.connect();
            stmt = connection.prepareStatement("select * from books limit ? offset ? ");
            stmt.setInt(1, size);
            stmt.setInt(2, (page - 1) * size);

            ResultSet rs = stmt.executeQuery();
            books = new ArrayList();

            int i = 0;
            while (rs.next()) {
                i++;
                Book book = new Book();
                book.setId(rs.getInt("id"));
                book.setPages(rs.getInt("pages"));
                book.setTitle(rs.getString("title"));
                book.setYear(rs.getInt("year"));

                books.add(book);
                LOG.info("Registro fila: " + i);
            }
            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return books;
    }

    public Book get(int id) {

        PreparedStatement stmt = null;
        Book book = null;
        try {
            this.connect();
            stmt = connection.prepareStatement("select * from books where id = ? ");
            stmt.setInt(1, id);

            ResultSet rs = stmt.executeQuery();
            rs.next();

            book = new Book();

            book.setId(rs.getInt("id"));
            book.setPages(rs.getInt("pages"));
            book.setTitle(rs.getString("title"));
            book.setYear(rs.getInt("year"));

            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return book;
    }

    public void insert(Book book) {
        PreparedStatement stmt = null;
        try {
            this.connect();
            stmt = connection.prepareStatement("INSERT INTO books(pages, title, year) VALUES(?, ? ,?)");
            stmt.setInt(1, book.getPages());
            stmt.setString(2, book.getTitle());
            stmt.setInt (3, book.getYear());
            
            
            stmt.execute();
            this.disconnect();
        } catch (SQLException ex) {
            LOG.log(Level.SEVERE, null, ex);
        }
        return;        
    }
    
    public void store(Book book) throws SQLException {
        PreparedStatement stmt = null;
        LOG.info("Crear DAO");
        this.connect();
        stmt = connection.prepareStatement(
                "INSERT INTO books(title, pages, year)"
                + " VALUES(?, ?, ?)"
        );
        //stmt.setLong(1, 4); // teacher.getId()
        
        //LOG.info(""+teacher.getId());
        stmt.setString(1, book.getTitle());
        stmt.setInt(2, book.getPages());
        stmt.setInt(3, book.getYear());

        stmt.execute();
        this.disconnect();
    }

}
