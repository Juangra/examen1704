/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlet;

import java.io.IOException;
import static java.lang.Integer.parseInt;
import static java.lang.Long.parseLong;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpSession;
import model.Book;
import persistence.BookDAO;

/**
 *
 * @author usuario
 */
public class BookController extends BaseController {
    
    private static final int ELEMENTS_PER_PAGE = 20;
    private static final Logger LOG = Logger.getLogger(BookController.class.getName());
    private BookDAO bookDAO;

    public void index(String page) {
        /*if(page==null){
        page="1";   
        }*/
       
        int pageNum = 1;
        //objeto persistencia
        bookDAO = new BookDAO();
        ArrayList<Book> books = null;
        
        try{
        pageNum = parseInt(page);
        }catch(Exception e){
            LOG.info("error");
        }
         
        //leer datos de la persistencia
        synchronized (bookDAO) {
            books = bookDAO.getAll(pageNum, ELEMENTS_PER_PAGE);
        } //para que no haya 2 conexiones a la vez
        request.setAttribute("books", books);
        String name = "index";
        LOG.info("En ModuleController->" + name);
//        LOG.info("Studies->" + studies.size());
        dispatch("/WEB-INF/view/book/index.jsp");
    }

    public void create() {
        dispatch("/WEB-INF/view/book/create.jsp");
    }

    public void store() throws IOException {

        //objeto persistencia
        bookDAO = new BookDAO();
        
        LOG.info("crear DAO");
        //crear objeto del formulario
        Book book = loadFromRequest();
        
        synchronized (bookDAO) {
            try {
                
                bookDAO.store(book);
                
            } catch (SQLException ex) {
                LOG.log(Level.SEVERE, null, ex);
                request.setAttribute("ex", ex);
                request.setAttribute("msg", "Error de base de datos ");
                dispatch("/WEB-INF/view/error/index.jsp");
                
            }
        }
        redirect(contextPath + "/book/index/1");
    }
   
    private Book loadFromRequest()
    {
        Book book = new Book();
        LOG.info("Crear modelo");
        book.setTitle(request.getParameter("title"));
        book.setPages(Integer.parseInt((request.getParameter("pages"))));
        book.setYear(Integer.parseInt((request.getParameter("year"))));

        
        LOG.info("Datos cargados");
        return book;
    }
    public void view(String idString) throws SQLException {
        LOG.info("idString");
        long id = toId(idString);
        BookDAO  bookDAO = new BookDAO();
        Book book = bookDAO.get((int) id);
        request.setAttribute("book", book);
       
        this.guardarSesion(idString);
        dispatch("/WEB-INF/view/book/show.jsp");

    }
    public void guardarSesion(String idString) throws SQLException {
        HttpSession session = request.getSession(true);
        session.setAttribute("id", idString); 
    }
    public void last(String idString) throws SQLException {
        redirect(contextPath + "/book/view/" + idString);
    }
}

